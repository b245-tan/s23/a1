// console.log("Hello World");


let trainer = {
	name: 'Ash Ketchum',
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},

	talk: function(thisPokemon){
		console.log(thisPokemon + "! I choose you!");
	}
}

console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of bracket notation:");
console.log(trainer["pokemon"]);

console.log("Result of talk method:");
trainer.talk("Pikachu");


function Pokemon(name, level){
	this.pokemonName = name;
	this.pokemonLevel = level;
	this.pokemonHealth = 2 * level;
	this.pokemonAttack = level;

	this.tackle = function(targetPokemon) {
		console.log(this.pokemonName + " used tackle on " + targetPokemon.pokemonName);
		let hpAftertackle = targetPokemon.pokemonHealth - this.pokemonAttack; 
		console.log(targetPokemon.pokemonName + " has been damaged by " + this.pokemonAttack);
		console.log(targetPokemon.pokemonName + "'s health is now reduced to " + hpAftertackle);

		this.faint = function(){
		console.log(this.pokemonName + " fainted!")
		}

		if(hpAftertackle <= 0){
			targetPokemon.faint();
		}

		console.log(targetPokemon);
	}
}

let pikachu = new Pokemon("Pikachu", 5);
console.log(pikachu);

let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

let mewtwo = new Pokemon("MewTwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);

mewtwo.tackle(geodude);

